package br.edu.up.projetodemapa;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity
    implements OnMapReadyCallback, LocationListener,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {

  public static final String TAG = MapsActivity.class.getSimpleName();
  private GoogleMap mapa;
  private GoogleMap mMap;
  private GoogleApiClient mGoogleApiClient;
  private Location mLastLocation;
  private Marker mCurrLocationMarker;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_maps);

    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      checkLocationPermission();
    }

    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    // Cria um cliente e vincula o método de retorno da conexão,
    // a escuta e o serviço de localização nele.
//    mGoogleApiClient = new GoogleApiClient.Builder(this)
//        .addConnectionCallbacks(this)
//        .addOnConnectionFailedListener(this)
//        .addApi(LocationServices.API)
//        .build();

    // Create the LocationRequest object
//    mLocationRequest = LocationRequest.create()
//        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//        .setInterval(10 * 1000)        // 10 seconds, in milliseconds
//        .setFastestInterval(1 * 1000); // 1 second, in milliseconds

//    GoogleApiAvailability gaa = GoogleApiAvailability.getInstance();
//    int retorno = gaa.isGooglePlayServicesAvailable(this);
//
//    if (retorno != ConnectionResult.SUCCESS) {
//
//      Dialog dialog = gaa.getErrorDialog(this, retorno, 0);
//      dialog.show();
//
//    } else {
//
//      FragmentManager fm = getSupportFragmentManager();
//      SupportMapFragment smf = (SupportMapFragment) fm.findFragmentById(R.id.map);
//      smf.getMapAsync(this);
//
//    }
  }


  @Override
  public void onMapReady(GoogleMap googleMap) {

    mMap = googleMap;
    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

    //Initialize Google Play Services
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (ContextCompat.checkSelfPermission(this,
          Manifest.permission.ACCESS_FINE_LOCATION)
          == PackageManager.PERMISSION_GRANTED) {
        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);
      }
    }
    else {
      buildGoogleApiClient();
      mMap.setMyLocationEnabled(true);
    }

//    mapa = googleMap;
//
//    mapa.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//    mapa.setMyLocationEnabled(true);
//
//    // Add a marker in Sydney and move the camera
//    LatLng sydney = new LatLng(-34, 151);
//    mapa.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//    mapa.moveCamera(CameraUpdateFactory.newLatLng(sydney));
  }

  protected synchronized void buildGoogleApiClient(){
    mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();
    mGoogleApiClient.connect();
  }

//  @Override
//  public void onMapReady(final GoogleMap googleMap) {
//
//    mapa = googleMap;
//
//    double latitudeInicial = -25.446828;
//    double longitudeInicial = -49.358875;
//
//   /*
//    Location localizacaoInicial = new Location("Localização inicial");
//    localizacaoInicial.setLatitude(latitudeInicial);
//    localizacaoInicial.setLongitude(longitudeInicial);
//
//    onLocationChanged(localizacaoInicial);
//
//    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.up_pin);
//
//    LatLng latLngInical = new LatLng(latitudeInicial, longitudeInicial);
//    MarkerOptions marker = new MarkerOptions();
//    marker.position(latLngInical);
//    marker.title("Estou aqui!");
//    marker.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
//    mapa.addMarker(marker);
//    mapa.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//    mapa.setMyLocationEnabled(true);
//    mapa.setOnMyLocationButtonClickListener(
//        new GoogleMap.OnMyLocationButtonClickListener() {
//          @Override
//          public boolean onMyLocationButtonClick() {
//
//            Location localizacaoAtual = getLocation();
//            onLocationChanged(localizacaoAtual);
//
//            return false;
//          }
//        });
//*/
//
//  }

//  public Location getLocation() {
//
//    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//    String provider = locationManager.getBestProvider(new Criteria(), true);
//    return locationManager.getLastKnownLocation(provider);
//
//  }

  public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

  public boolean checkLocationPermission(){
    if (ContextCompat.checkSelfPermission(this,
        Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {

      // Asking user if explanation is needed
      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
          Manifest.permission.ACCESS_FINE_LOCATION)) {

        // Show an expanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.

        //Prompt the user once explanation has been shown
        ActivityCompat.requestPermissions(this,
            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
            MY_PERMISSIONS_REQUEST_LOCATION);


      } else {
        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(this,
            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
            MY_PERMISSIONS_REQUEST_LOCATION);
      }
      return false;
    } else {
      return true;
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
       String permissions[], int[] grantResults) {
    switch (requestCode) {
      case MY_PERMISSIONS_REQUEST_LOCATION: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

          // Permission was granted.
          if (ContextCompat.checkSelfPermission(this,
              Manifest.permission.ACCESS_FINE_LOCATION)
              == PackageManager.PERMISSION_GRANTED) {

            if (mGoogleApiClient == null) {
              buildGoogleApiClient();
            }
            mMap.setMyLocationEnabled(true);
          }

        } else {

          // Permission denied, Disable the functionality that depends on this permission.
          Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
        }
        return;
      }

      // other 'case' lines to check for other permissions this app might request.
      //You can add here other case statements according to your requirement.
    }
  }


  @Override
  public void onLocationChanged(Location location) {

    mLastLocation = location;
    if (mCurrLocationMarker != null) {
      mCurrLocationMarker.remove();
    }

    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bike);

    //Place current location marker
    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    MarkerOptions markerOptions = new MarkerOptions();
    markerOptions.position(latLng);
    markerOptions.title("Current Position");
    //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
    mCurrLocationMarker = mMap.addMarker(markerOptions);

    //move map camera
    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

    //stop location updates
    //if (mGoogleApiClient != null) {
    //  LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    //}

//    handleNewLocation(location);

//    float zoom = mapa.getCameraPosition().zoom;
//    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//
//    CameraUpdate position = CameraUpdateFactory.newLatLng(latLng);
//    CameraUpdate zoomTo = CameraUpdateFactory.zoomTo(zoom);
//
//    mapa.moveCamera(position);
//    mapa.animateCamera(zoomTo);
  }


  @Override
  public void onConnected(@Nullable Bundle bundle) {

    mLocationRequest = new LocationRequest();
    mLocationRequest.setInterval(1000);
    mLocationRequest.setFastestInterval(1000);
    mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    if (ContextCompat.checkSelfPermission(this,
        Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED) {
      LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

//    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//    if (location == null) {
//      LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//    } else {
//      handleNewLocation(location);
//    }
  }

  private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

  private LocationRequest mLocationRequest;

  private void handleNewLocation(Location location) {
    double currentLatitude = location.getLatitude();
    double currentLongitude = location.getLongitude();
    LatLng latLng = new LatLng(currentLatitude, currentLongitude);

    MarkerOptions options = new MarkerOptions()
        .position(latLng)
        .title("I am here!");
    mapa.addMarker(options);
    mapa.moveCamera(CameraUpdateFactory.newLatLng(latLng));
  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//    if (connectionResult.hasResolution()) {
//      try {
//        connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
//      } catch (IntentSender.SendIntentException e) {
//        e.printStackTrace();
//      }
//    } else {
//      Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
//    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    //setUpMap();
    //mGoogleApiClient.connect();
  }

  @Override
  protected void onPause() {
    super.onPause();
//    if (mGoogleApiClient.isConnected()) {
//      LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//      mGoogleApiClient.disconnect();
//    }
  }

  private void setUpMap() {
    mapa.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
  }
}